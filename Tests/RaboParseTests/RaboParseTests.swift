//
//  RaboParseTests.swift
//  RaboParseTests
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import RaboParse

class RaboParseSpec: QuickSpec {
    override func spec() {
        
        var app: ((Message) -> ())!
        
        var received: [Message]!
        var expectedRows: [IssueRow]!
        
        beforeEach {
            received = []
            expectedRows = [
                IssueRow(firstName: "Theo", surName: "Jansen", count: 5, birthday: Calendar.current.date(from: {
                    var comps = DateComponents()
                    comps.year = 1978
                    comps.month = 1
                    comps.day = 2
                    return comps
                }())!),
                IssueRow(firstName: "Fiona", surName: "de Vries", count: 7, birthday: Calendar.current.date(from: {
                    var comps = DateComponents()
                    comps.year = 1950
                    comps.month = 11
                    comps.day = 12
                    return comps
                }())!),
                IssueRow(firstName: "Petra", surName: "Boersma", count: 1, birthday: Calendar.current.date(from: {
                    var comps = DateComponents()
                    comps.year = 2001
                    comps.month = 4
                    comps.day = 20
                    return comps
                }())!)
            ]
            let mockReceiver: (Message) -> () = { msg in
                received = received + [msg]
         
                if case .issue(.deleted(_)) = msg {
                    app(.state(.fetch))
                }
            }
            app = createApp(rootHandler: { app?($0) }, receivers: [mockReceiver])
            app(.appStart(.initialize))
        }
        
        afterEach {
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("issues.csv")
            try? FileManager.default.removeItem(at: url)
            expectedRows = nil
            received = nil
            app = nil
        }
        
        describe("parse Feature") {
            context("parsing") {
                it("successful parsing results in finish msg") {
                    app(.parse(.start))
                    app(.state(.fetch))
                    expect(received).toEventually(contain([.parse(.start), .parse(.finished(expectedRows))]))
                }
            }
        }
        
        describe("state feature") {
            context("writing") {
                
                it("results in written msg"){
                    app(.state(.write(expectedRows)))
                    
                    expect(received).toEventually(contain([.state(.write(expectedRows)), .state(.written(expectedRows))]))
                }
            }
            
            context("loading") {
                
                beforeEach {
                    app(.state(.write(expectedRows)))
                }
                it("loads all rows"){
                    app(.state(.fetch))
                    
                    expect(received).toEventually(contain([.state(.fetch), .state(.fetched(.success(expectedRows)))]))
                }
            }
        }
        
        describe("issue feature") {
                
            var received: [Message]!
            var fetchedRows: [IssueRow]!
            
            beforeEach {
                received = []
                fetchedRows = []
                
                let mockReceiver: (Message) -> () = { msg in
                    received = received + [msg]
                    
                    if case .appStart(.wasInitialized) = msg {
                        app(.state(.fetch))
                    }
                    
                    if case .state(.fetched(.success(let rows))) = msg {
                          fetchedRows = rows
                    }
                    
                    
                    if case .issue(.deleted(_)) = msg {
                        app(.state(.fetch))
                    }
                }
                
                app = createApp(rootHandler: { app?($0) }, receivers: [mockReceiver])
                app(.appStart(.initialize))
            }
            
            afterEach {
                fetchedRows = nil
                app = nil
            }
            it("sends a deletion msg on deletion success") {
                app(.issue(.delete(expectedRows[0])))
                
                expect(received).toEventually(contain([.issue(.delete(expectedRows[0])), .issue(.deleted(expectedRows[0]))]))
            }
            
            it("removes deleted issue from fetched issues") {

                app(.state(.fetch))
                app(.issue(.delete(expectedRows[0])))

                expect(fetchedRows).toEventually(equal(expectedRows.filter { $0 != expectedRows[0] }), timeout: 5)
            }
        }
    }
}

extension Message: Equatable {
    public static func == (lhs: Message, rhs: Message) -> Bool {
        switch (lhs, rhs) {
        case ( .parse(.start),              .parse(.start)             ) : return true
        case ( .parse(.finished(let lrow)), .parse(.finished(let rrow))) : return lrow == rrow
        case ( .parse(.failed(let lError)), .parse(.failed(let rError))) : return lError == rError
        case ( .state(.write(let lRows))  , .state(.write(let rRows))  ) : return lRows == rRows
        case ( .state(.written(let lRows)), .state(.written(let rRows))) : return lRows == rRows
        case (.state(.fetched(.failed(_))), .state(.fetched(.failed(_)))): return true
        case (.state(.fetch), .state(.fetch)): return true
        case (.state(.fetched(.success(let lRows))), .state(.fetched(.success(let rRows)))): return lRows == rRows
        case (.issue(.delete(let lRow)), .issue(.delete(let rRow))): return lRow == rRow
        case (.issue(.deleted(let lRow)), .issue(.deleted(let rRow))): return lRow == rRow

        default:
            return false
        }
    }
}
