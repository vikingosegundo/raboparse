//
//  CSVFileLoader.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import CSV

struct CSVFileLoader {
    func load(url: URL) -> Result<[IssueRow], LoadCSVFileError>{
        guard
            let stream = InputStream(url: url),
            let csv = try? CSVReader(stream: stream, hasHeaderRow: true)
            else {
                return .failure(LoadCSVFileError.fileNotFound(url))
        }
        let rows = csv.reduce([]) { $0 + [IssueRow(firstName: $1[0], surName: $1[1], count: Int($1[2])!, birthday: df.date(from: $1[3])!)] }
        return .success(rows)
    }
}
