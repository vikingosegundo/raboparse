//
//  IssueFeature.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func createIssueFeature(allRowsAccessor: AllRowsAccessor,
                                 output: @escaping Output) -> Input
{
    let deleteIssueRow = DeleteIssueRow(issueRowDeleter: IssueRowDeleter(allRowsAccessor: allRowsAccessor, output: output)){ (response) in
        switch response {
        case .deleted(let row): output(.issue(.deleted(row)))
        }
    }
    
    let exit: (Message) -> () = { output($0) }
    
    return { msg in
        if case .issue(.delete(let row)) = msg { deleteIssueRow.request(.delete(row)) }
        if case .issue(.deleted(_))      = msg { exit( .state(.fetch) ) }
    }
}
