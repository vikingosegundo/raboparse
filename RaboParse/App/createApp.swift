//
//  createApp.swift
//  RaboParse
//
//  Copyright © 2020 Manuel. All rights reserved.
//

import Foundation

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createApp(receivers: [Input],
             rootHandler: @escaping Output) -> Input
{
    let fileURL = getFileURL()
    let allRows = AllRowsAccessor()
    let hooks: [Input] = [allRows.input]
    
    let features: [Input] = [
        createAppStartFeature (fileURL: fileURL, output: rootHandler),
        createParseFeature    (fileURL: fileURL, fileLoader: CSVFileLoader(), userDefaults: UserDefaults.standard, output: rootHandler),
        createStateFeature    (fileURL: fileURL, fileLoader: CSVFileLoader(), fileWriter: CSVFileWriter(), output: rootHandler),
        createIssueFeature    (allRowsAccessor: allRows, output: rootHandler)
    ]
    return { msg in
        (receivers + hooks + features).forEach { $0(msg) }
    }
}

func getFileURL() -> (URL) {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("issues.csv")
}
