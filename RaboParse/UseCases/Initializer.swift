//
//  Initializer.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
    
struct Initializer: UseCase {
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request  { case initialize     }
    enum Response { case wasInitialized }
    
    init(fileURL:URL, responder: @escaping (Response) -> Void) {
        self.respond = responder
        self.fileURL = fileURL
    }
    
    func request(_ request: Request) {
        switch request {
        case .initialize:
            let url = Bundle.main.url(forResource: "issues", withExtension: "csv")!
            if !FileManager.default.fileExists(atPath: fileURL.absoluteString) {
                try? FileManager.default.copyItem(at: url, to: fileURL)
            }
            respond(.wasInitialized)
        }
    }
    
    private let respond: ((Response) -> ())
    private let fileURL: URL
}
